---
layout: job_page
title: "Technical Writing Lead"
---

## Responsibilities

* Lead documentation improvement efforts
* Build out GitLab University so that everyone can quickly get started with GitLab
* Maintain a single source of truth across our sites, documentation and guides
* Create practices that encourage keeping documentation up to date and easily discoverable
* Ensure that the product content on the website is comprehensive and up to date
* Create a flow of guides on GitLab's defining features
* Lead the technical writers team
* Deliver input on promotions, function changes, demotions and firings in consultation with the CEO, and VP of Product
* Make sure by the time of release all documentation, guides and content
on the website is up to date.
